﻿using System;

namespace PublishSubscribe
{
    public class Subscriber1 : IDisposable
    {
        private readonly IPublisher _p;

        public Subscriber1(IPublisher p)
        {
            _p = p;
            p.EnterPressed += OnEnterPressed;
        }

        private void OnEnterPressed(object sender, EnterPressedArgs eventArgs)
        {
            Console.WriteLine($"wpisano: {eventArgs.Text}, subskrybentów: {eventArgs.SubscriberCount}");
        }

        public void Dispose()
        {
            _p.EnterPressed -= OnEnterPressed;
        }
    }

    public class Subscriber2
    {
        public Subscriber2(IPublisher p)
        {
            p.EnterPressed += (sender, args) => Console.WriteLine($"{DateTime.Now:T}: naciśnięto enter");
        }
    }
}
