﻿using System;

namespace PublishSubscribe
{
    public interface IPublisher
    {
        event EventHandler<EnterPressedArgs> EnterPressed;
    }

    public class EnterPressedArgs : EventArgs
    {
        public EnterPressedArgs(string text, int subscriberCount)
        {
            Text = text;
            SubscriberCount = subscriberCount;
        }

        public string Text { get; }

        public int SubscriberCount { get; }
    }

    public class Publisher : IPublisher
    {
        private EventHandler<EnterPressedArgs> _enterPressed;
        private int subscriberCount = 0;
        public event EventHandler<EnterPressedArgs> EnterPressed
        {
            add
            {
                _enterPressed += value;
                ++subscriberCount;
            }
            remove
            {
                _enterPressed -= value;
                --subscriberCount;
            }
        }

        public void Run()
        {
            while (true)
            {
                string text = Console.ReadLine();
                _enterPressed?.Invoke(this, new EnterPressedArgs(text, subscriberCount));
            }
        }
    }
}
