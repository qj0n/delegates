﻿
using System;
using System.Timers;

namespace PublishSubscribe
{
    public class SimulatingPublisher : IPublisher, IDisposable
    {
        private readonly Timer _timer;
        public event EventHandler<EnterPressedArgs> EnterPressed;

        public SimulatingPublisher()
        {
            _timer = new Timer(5000);
            _timer.Elapsed += (sender, args) =>
                EnterPressed?.Invoke(this, new EnterPressedArgs("test", 42));

            _timer.Start();
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}
