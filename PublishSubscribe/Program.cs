﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PublishSubscribe
{
    class Program
    {
        static void Main(string[] args)
        {
            //var p = new Publisher();
            using (var p = new SimulatingPublisher())
            {
                var s = new Subscriber1(p);
                var s2 = new Subscriber1(p);
                var s3 = new Subscriber2(p);

                s2.Dispose();

//            p.Run();
                Console.ReadLine();
            }
        }
    }
}
