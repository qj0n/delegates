﻿using System;
using NUnit.Framework;

namespace Delegates
{

    [TestFixture]
    public class DelegateTests
    {
        public delegate int StringToInt(string s);

        public delegate string StringProvider();

        public delegate void VoidDelegate();

        public void AssertThrows(VoidDelegate code)
        {
            if (code == null)
                throw new ArgumentNullException();
            try
            {
                code.Invoke();
            }
            catch
            {
                return;
            }
            throw new Exception();
        }

        [Test]
        public void AssertTests()
        {
            VoidDelegate exceptionDelegate = () => throw new Exception();

            Assert.DoesNotThrow(
                () =>
                    AssertThrows(exceptionDelegate)
                );
        }

        [Test]
        public void AssertTests2()
        {
            VoidDelegate noExceptionDelegate = () => { };

            Assert.Throws<Exception>(
                () =>
                    AssertThrows(noExceptionDelegate)
            );
        }

        [Test]
        public void AssertTests3()
        {
            Assert.Throws<ArgumentNullException>(
                () =>
                    AssertThrows(null)
            );
        }

        [Test]
        public void CallDelegate()
        {
            StringToInt s = (string str) => int.Parse(str);
            var res = s("12");

            Assert.That(res, Is.EqualTo(12));
        }

        [Test]
        public void CallDelegate2()
        {
            StringToInt s = int.Parse;

            var res = s("12");

            Assert.That(res, Is.EqualTo(12));
        }


        [Test]
        public void CallDelegate3()
        {
            int a = 15;
            StringProvider provider = a.ToString;

            a = 10;
            var res = provider?.Invoke();

            Assert.That(res, Is.EqualTo("15"));
        }

        [Test]
        public void CallDelegate4()
        {
            int a = 15;
            StringProvider provider = () => a.ToString();

            a = 10;
            var res = provider();

            Assert.That(res, Is.EqualTo("10"));
        }

        [Test]
        public void CallDelegate5()
        {
            StringProvider provider = default(StringProvider);

            Assert.Throws<NullReferenceException>(() => provider());
        }

        [Test]
        public void CallDelegate6()
        {
            Func<string> provider = null;

            Assert.Throws<NullReferenceException>(() => provider());
        }

        [Test]
        public void CallDelegate7()
        {
            Func<string, int> s = (string str) => int.Parse(str);
            var res = s("12");

            Assert.That(res, Is.EqualTo(12));
        }

        [Test]
        public void CallDelegate8()
        {
            Action<string> s = (string str) => int.Parse(str);
            s("12");
        }
    }
}
