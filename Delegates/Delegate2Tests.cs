﻿using System;
using NUnit.Framework;

namespace Delegates
{
    [TestFixture]
    class Delegate2Tests
    {
        [Test]
        public void SumDelegates()
        {
            int counter = 0;
            Action a = () =>
            {
                counter++;
            };
            Action sum = a + a;

            sum();

            Assert.That(counter, Is.EqualTo(2));
        }

        [Test]
        public void SumDelegates2()
        {
            int counter = 0;
            Action a = () => counter++;
            Action sum = (a + a) - a - a;

            sum?.Invoke();

            Assert.That(counter, Is.EqualTo(0));
        }

        [Test]
        public void SumDelegates3()
        {
            int counter = 0;
            Action a = () => counter++;
            Action b = () => counter++;
            Action sum = a + a - b;

            sum?.Invoke();

            Assert.That(counter, Is.EqualTo(2));
        }

        [Test]
        public void SumDelegates4()
        {
            int counter = 0;
            Action a = () => counter++;
            Action b = () => counter++;
            Action sum = (a + (a+b)) - (b+(a+a));

            sum?.Invoke();

            Assert.That(counter, Is.EqualTo(3));
        }

        [Test]
        public void SumDelegates5()
        {
            int counter = 0;
            void a()
            {
                counter++;
            }

            Action b = a;
            Action sum = (Action) a + a - b;

            sum?.Invoke();

            Assert.That(counter, Is.EqualTo(1));
        }
    }
}
